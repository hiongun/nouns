#!/bin/env python
# -*- coding: utf-8 -*-

import sys, re

def DEBUG_PRINT(str_line):
#{
    import inspect
    sys.stdout.write("(%s,%d) %s\n" % (__file__, inspect.stack()[1][2], str_line.strip()))
#}

def load_dict():
#{
    n_words = {}
    j_words = {}

    fp = open("./dict/dict_system.tsv", "r")

    for line in fp.readlines():
        kv = line.strip().split("\t")
        for i in range(1, len(kv)):
            if kv[i][0:1] == 'n':
                n_words[kv[0]] = kv[i][0:1]
            if kv[i][0:1] == 'j':
                j_words[kv[0]] = kv[i][0:1]
            # print("'%s', '%s'" % (kv[0], kv[1][0:1]))
    fp.close()

    return (n_words, j_words)
#}


def get_nouns_in(n_words, j_words, line):
#{
    nouns = []

    # for splitted in line.strip().split(" "):
    # for splitted in re.split('[\(\)&,”:;\- `\!\'\"\?\.]', line.strip()):
    for splitted in re.split('[\(\)&,”:;\- `\!\'\"\?\.]', line.strip()):
        # DEBUG_PRINT(splitted)
        splitted = splitted.strip()
        n = len(splitted)
        for i in range(0, n-1):
            head = splitted[0:n-i]
            tail = splitted[n-i:]

            # DEBUG_PRINT("%s + %s" % (head, tail))

            if tail == "":
                if head in n_words.keys():
                    # print("NOUN: %s" % (head))
                    nouns.append(head)
            else:
                if head in n_words.keys() and tail in j_words.keys():
                    # print("NOUN: %s" % (head))
                    nouns.append(head)

    # DEBUG_PRINT("nouns: %s" % (nouns))

    return nouns
#}


n_words = None
def get_words(line):
#{
    global n_words, j_words

    if n_words is None:
        (n_words, j_words) = load_dict()

    # DEBUG_PRINT("load_dict() ok")

    return get_nouns_in(n_words, j_words, line)
#}

if __name__ == "__main__":
#{
    if len(sys.argv) < 2:
        print("Usage: %s text" % (sys.argv[0]))
        sys.exit(0)

    line = sys.argv[1]

    nouns = get_words(line)
    for noun in nouns:
        print("'%s'" % (noun))
#}
