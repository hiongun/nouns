**Pure Python Korean Noun Extractor**

순수 파이선으로 구현된 한글 명사 추출기

**Usage:**

import nouns

text = "텍스트 스트링 ..."

words = nouns.get_words(text)

**Required:**

./dict 디렉토리에 텍스트로 된 사전이 필요함